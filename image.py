from __future__ import division
from PIL import Image
import numpy
import sqlite3
db_file = raw_input("Database filename: ")
conn = sqlite3.connect(db_file)
c = conn.cursor()
image_height = 50
image_height_step = int(raw_input("IMG HEIGHT STEPSIZE: "))
image_width = int(raw_input("IMG WIDTH (AZ WIDTH): "))
image_width_step = int(raw_input("IMG WIDTH STEPSIZE: "))
pixels = []
y = image_height
x = image_width - (image_width / 2)
buffer = []

#GET BRIGHTEST AND DARKEST PIXEL VALUES
c.execute("SELECT voltage FROM dishData WHERE voltage = (SELECT min(voltage) FROM dishData)")
darkest = int(c.fetchall()[0][0])
c.execute("SELECT voltage FROM dishData WHERE voltage = (SELECT max(voltage) FROM dishData)")
brightest = int(c.fetchall()[0][0])
adj = int(brightest - darkest)
for i in range (int(image_height / image_height_step)):
    for i in range(int(image_width / image_width_step) + 1):
        #print "X : " + str(x)
        #print "Y : " + str(y)
        c.execute("SELECT voltage FROM dishData WHERE altitude=(?) AND azimuth=(?)", ((y + 40), x))
        data = c.fetchall()
        data = int(data[0][0])
        data = ((data - darkest) * 255) / adj
        list(buffer)
        buffer.append((data, data, data))
        tuple(buffer)
        #print "BUFF: " + str(buffer)
        x = x - image_width_step
    list(pixels)
    pixels.append(buffer)
    tuple(pixels)
    y = y - image_height_step
    x = image_width - (image_width / 2) #reset x cursor
    buffer = [] #reset buffer


#print pixels
array = numpy.array(pixels, dtype=numpy.uint8)

new_image = Image.fromarray(array)
new_image.save('image.png')

