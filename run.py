from __future__ import division
from time import sleep
import pigpio
from RESOURCES import ascii_art
import os
import serial
import sqlite3
import datetime
import serial

#AZIMUTH
DIR_az = 20
STEP_az = 21
#ALTITUDE
DIR_alt = 19
STEP_alt = 26
#REPLAYS
RELAY_alt = 23
RELAY_az = 24
#LOCATION TRACKING
loc = 90
loc_az = 0
#CONFIG
be_verbose = True
#SETUP SERIAL
ser = serial.Serial('/dev/ttyUSB0', 9600)
#AVERAGING
avg_amount = 5
#SETUP SQLITE DATABASE FOR STORING COLLECTED INFORMATION
#now = datetime.datetime.now()
print "WARNING: RUNNING THIS WILL DELETE ANY DATABASE FILES IN THIS DIRECTORY"
raw_input("PRESS ENTER TO CONTINUE")
db_file = "dbfile.db" #+ str(datetime.time(now.hour, now.minute, now.second)) + ".db"
conn = sqlite3.connect(db_file)
c = conn.cursor()
c.execute("CREATE TABLE IF NOT EXISTS dishData(voltage FLOAT, azimuth FLOAT, altitude FLOAT)")

# Connect to pigpiod daemon
pi = pigpio.pi()

# Set up pins as an output
pi.set_mode(DIR_az, pigpio.OUTPUT)
pi.set_mode(STEP_az, pigpio.OUTPUT)
pi.set_mode(DIR_alt, pigpio.OUTPUT)
pi.set_mode(STEP_alt, pigpio.OUTPUT)
pi.set_mode(RELAY_alt, pigpio.OUTPUT)
pi.set_mode(RELAY_az, pigpio.OUTPUT)

MODE_az = (14, 15, 18)
MODE_alt = (17, 27, 4)
RESOLUTION = {'Full': (0, 0, 0),
'Half': (1, 0, 0),
'1/4': (0, 1, 0),
'1/8': (1, 1, 0),
'1/16': (0, 0, 1),
'1/32': (1, 0, 1)}

for i in range(3):
    pi.write(MODE_az[i], RESOLUTION['1/32'][i])
    pi.write(MODE_alt[i], RESOLUTION['Half'][i])

def toggle_ALT(pin):
    pi.write(RELAY_alt, pin)

def toggle_AZ(pin):
    pi.write(RELAY_az, pin)

def move_AZ(dir, degrees):
    global loc_az
    #106 seconds 360 degrees
    val1 = (degrees * 10)
    val2 = ((360 * (1 / 106)) / (1 / 10))
    duration = val1 / val2
    freq = 320
    dc = 10
    toggle_AZ(1)
    pi.set_PWM_dutycycle(STEP_az, dc)
    pi.set_PWM_frequency(STEP_az, freq)
    pi.write(DIR_az, dir)
    sleep(duration)
    pi.set_PWM_dutycycle(STEP_az, 0)
    if dir == 1:
        loc_az = loc_az + degrees
    else:
        loc_az = loc_az - degrees
    if verbosity:
        print "\r[*] AZIMUTH (deg) : " + str(loc_az)

def move_ALT(dir, degrees):
    global loc
    while True:
        #CHECK:
        if dir == 1 and (loc - degrees) < 40:
            print "[!] ERROR"
            break
        elif dir == 0 and (loc + degrees > 90):
            print "[!] ERROR"
            break
        elif dir == 1:
            loc = loc - degrees
        elif dir == 0:
            loc = loc + degrees
        elif dir == 2 or dir == 3:
            if dir == 2:
                dir = 0
            elif dir == 3:
                dir = 1
        freq = 320
        dc= 10
        #50 deg (40-90) = 35.71s --> 1 deg = 0,7142
        duration = degrees * 0.7142
        toggle_ALT(1)
        pi.set_PWM_dutycycle(STEP_alt, dc)
        pi.set_PWM_frequency(STEP_alt, freq)
        pi.write(DIR_alt, dir)
        sleep(duration)
        pi.set_PWM_dutycycle(STEP_alt, 0)
        if verbosity:
            print "\r[*] ALTITUDE (deg) : " + str(loc)
        toggle_ALT(0)
        break

def db_update(new_az, voltage, altitude_ref):
    c.execute("INSERT INTO dishData (voltage, azimuth, altitude) VALUES(?, ?, ?)", (voltage, new_az, altitude_ref))
    conn.commit()

def readVoltage():
    total = 0
    iterations = 1
    while True:
        read = ser.readline()
        if read.isspace() == False:
            read = read.replace('\r', '')
            read = read.replace('\n', '')
            read = read.split(".")[0]
            total = total + int(float(read))
            if iterations == avg_amount:
                break
            iterations = iterations + 1
    voltage = total / avg_amount
    print "[*] POWER : " + str(voltage * 10)
    return int(voltage * 10)

def scan(az_width, az_stop, alt_stop):
    last_az_dir = 1
    location = 90
    move_AZ(1, (az_width/2))
    voltage = readVoltage()
    db_update(loc_az, voltage, loc)
    for i in range(int(50 / int(alt_stop))):
        if last_az_dir == 1:
            for i in range(int((az_width/az_stop))):
                move_AZ(0, az_stop)
                voltage = readVoltage()
                db_update(loc_az, voltage, loc)
                sleep(0.5)
            last_az_dir = 0
        elif last_az_dir == 0:
            for i in range(int((az_width/az_stop))):
                move_AZ(1, az_stop)
                voltage = readVoltage()
                db_update(loc_az, voltage, loc)
                sleep(0.5)
            last_az_dir = 1
        move_ALT(1, alt_stop)
        sleep(0.5)
        location = (90 - alt_stop)
        voltage = readVoltage()
        db_update(loc_az, voltage, loc)
    if last_az_dir == 1:
        for i in range(int(az_width/az_stop)):
            move_AZ(0, az_stop)
            voltage = readVoltage()
            db_update(loc_az, voltage, loc)
            sleep(0.5)
        last_az_dir = 0
    elif last_az_dir == 0:
        for i in range(int(az_width/az_stop)):
            move_AZ(1, az_stop)
            voltage = readVoltage()
            db_update(loc_az, voltage, loc)
            sleep(0.5)
        last_az_dir = 1
    move_AZ(0, (90 - location))

try:
    verbosity = False
    #CALIBRATION
    os.system("clear")
    ascii_art.title()
    print "[*] Testing steppers..."
    sleep(1)
    move_AZ(1, 10)
    sleep(0.5)
    move_AZ(0, 10)
    sleep(0.5)
    move_ALT(1, 3)
    sleep(0.5)
    move_ALT(0, 3)
    sleep(0.5)
    if be_verbose == True:
        verbosity = True
    else:
        verbosity = False
    position = int(raw_input("[*] What's the current position of the dish? (deg) : "))
    move_ALT(2, ((90 - position) + 10))
    move_ALT(3, 10)

    #INPUT DATA
    os.system("clear")
    ascii_art.title()
    print "[*] INPUT SCAN PARAMETERS"
    az_width = int(raw_input("[!] AZ_WIDTH (deg) : "))
    az_stop = int(raw_input("[!] AZ_STOP (deg) : "))
    alt_stop = int(raw_input("[!] ALT_STOP (deg) : "))
    #START SCAN
    os.system("clear")
    ascii_art.title()
    print "[*] AZ_WIDTH = " + str(az_width) + " deg"
    print "[*] AZ_STOP = " + str(az_stop) + " deg"
    print "[*] ALT_STOP = " + str(alt_stop) + " deg"
    scan(az_width, az_stop, alt_stop)

except KeyboardInterrupt:
    print ("\n[!] Ctrl-C pressed.  Stopping PIGPIO and exiting...")
finally:
    pi.set_PWM_dutycycle(STEP_az, 0)  # PWM off
    pi.set_PWM_dutycycle(STEP_alt, 0)  # PWM off
    toggle_AZ(0)
    toggle_ALT(0)
    pi.stop()
