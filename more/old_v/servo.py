from __future__ import division
import math
import RPi.GPIO as GPIO
import time
import Adafruit_PCA9685

relay = 16 #Pin number for relay
maxi = 180
step = 0.01
sleep_time = 0.0001
ub = 600  # Min pulse length out of 4096
lb = 210  # Max pulse length out of 4096
smooth = True

GPIO.setmode(GPIO.BOARD)
GPIO.setup(relay,GPIO.OUT)
GPIO.output(relay,GPIO.HIGH)
pwm = Adafruit_PCA9685.PCA9685()
pwm.set_pwm_freq(60)
pwm.set_pwm(0, 0, int(lb))
pwm.set_pwm(1, 0, int(lb))
last_position_chan0 = lb
last_position_chan1 = lb
calc_duty = (ub - lb) / (180 - 0)

def setAngle(angle, smooth, chan):
    global last_position_chan0
    global last_position_chan1
    if chan == 0:
	    if angle > last_position_chan0:
	        movement = True
	    elif angle < last_position_chan0:
	        movement = False
	    elif angle == 0:
	        movement = None
    elif chan == 1:
            if angle > last_position_chan1:
                movement = True
            elif angle < last_position_chan1:
                movement = False
            elif angle == 0:
                movement = None

    if not smooth:
        duty = ((angle / maxi) * (ub - lb)) + lb
        print angle
        print duty
        pwm.set_pwm(chan, 0, int(duty))
	if chan == 0:
        	last_position_chan0 = duty
	elif chan == 1:
		last_position_chan1 = duty

    if smooth:
        duty = ((angle / maxi) * (ub - lb)) + lb
	if chan == 0:
	        if last_position_chan0 != duty:
	            if angle == 0 and last_position_chan0 == lb:
	                pass
	            else:
	                move_servo_smooth(last_position_chan0, duty, chan)
	                last_position_chan0 = duty
        if chan == 1:
                if last_position_chan1 != duty:
                    if angle == 0 and last_position_chan1 == lb:
                        pass
                    else:
                        move_servo_smooth(last_position_chan1, duty, chan)
                        last_position_chan1 = duty

def move_servo_smooth(start, end, chan):
    b_diff = end - float(start)
    x = 0
    while True:
        y = math.sin(x + (- math.pi/2)) * (b_diff/2) + (end - (b_diff/2))
        x = x + step
        print "Y: " + str(y)
        if x > math.pi:
            break
        else:
            pwm.set_pwm(chan, 0, int(y))
            time.sleep(sleep_time)

def loop():
    while True:
        angle = raw_input("ANGLE1: ")
        if angle.isspace() == True or int(angle) > maxi or int(angle) < 0:
            pass
        else:
            setAngle(int(angle), smooth, 0)
	angle = raw_input("ANGLE2: ")
	if angle.isspace() == True or int(angle) > maxi or int(angle) < 0:
	    pass
	else:
	    setAngle(int(angle), smooth, 1)
try:
    loop()
    time.sleep(1)
    GPIO.output(relay,GPIO.LOW)
    GPIO.cleanup()
except KeyboardInterrupt:
    GPIO.output(relay,GPIO.LOW)
    GPIO.cleanup()
